const prompt = require('prompt-sync')();


class Product {
  name
  price
  amount

  total = 0

  constructor(name, price, amount) {
    this.name = name;
    this.price = price;
    this.amount = amount;

    this.total += this.price * this.amount;
  }
}

class Order {
  items
  state

  subTotal = 0
  discount = 0
  tax = 0
  total = 0

  constructor(items, state) {
    this.items = items;
    this.state = state;

    this.subTotal = this.items.reduce((partial, item) => partial + item.total, 0)
    this.total = this.subTotal
  }

  applyTax() {
    this.tax = this.total * this.state[1]
    this.total += this.tax
    return this
  }

  applyDiscount() {
    let d = 0

    discounts.forEach(disc => {
      if (disc[0] <= this.total) d = disc[1]
    })

    this.discount = this.total * d
    this.total -= this.discount
    return this
  }
}

const stateTax = [
  [ "UT", 0.0685 ],
  [ "NV", 0.08 ],
  [ "TX", 0.0625 ],
  [ "AL", 0.04 ],
  [ "CA", 0.0825 ]
]

const discounts = [
  [ 1000, 0.03 ],
  [ 5000, 0.05 ],
  [ 7000, 0.07 ],
  [ 10000, 0.1 ],
  [ 50000, 0.15 ]
]

const addProduct = () => {
  var onGoing = true;
  var name = ""
  while(onGoing){
    name = prompt('What is the product name? ');
    if(name == ''){
      console.log('Please input a valid name');
    }
    else {
      onGoing = false
    }
  }
  
  onGoing = true;
  var price = 0;
  while(onGoing){
    price = +prompt('What is the product price? ');
    if(price == '' || price < 0){
      console.log('Please input a valid price');
    }
    else {
      onGoing = false
    }
  }
  
  onGoing = true;

  const amount = +prompt('How many do you want?? ');
  
  
  return new Product(name, price, amount);
  
}

const products = []

var onGoing = true;

while (onGoing) {
  products.push(addProduct())
  console.log(' y for yes n for no')
  const ans = prompt('Do you want to add anotgher product to the order? ');
  if(ans == 'n'){
    onGoing = false;
  }
}


var state = prompt('What state are you in? ');
var tax = stateTax.find(st => st[0].includes(state));
while (!tax) {
  state = prompt('What state are you in? write it well bruh ');
  tax = stateTax.find(st => st[0].includes(state));
}



const order = new Order(products, tax).applyDiscount().applyTax()
console.log("Sub-total: ", order.subTotal)
console.log("Discount: ", order.discount)
console.log("Tax: ", order.tax)
console.log("Total: ", order.total)
